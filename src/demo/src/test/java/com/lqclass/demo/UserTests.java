package com.lqclass.demo;

import com.lqclass.demo.controller.UserController;
import com.lqclass.demo.domain.User;
import com.lqclass.demo.utils.JsonData;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

@SpringBootTest
public class UserTests {

    @Autowired
    private UserController userController;

    @Test
    public void loginTest(){
        User user = new User();
        user.setUsername("dotnet9");
        user.setPwd("123");

        JsonData jsonData = userController.login(user);
        System.out.println(jsonData.toString());

        Assert.isTrue(jsonData.getCode()==0, "登录成功");
    }
}

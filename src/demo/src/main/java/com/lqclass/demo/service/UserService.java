package com.lqclass.demo.service;

import com.lqclass.demo.domain.User;

import java.util.List;

public interface UserService {
    public String login(String username, String pwd);

    public List<User> list();
}

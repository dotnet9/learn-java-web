package com.lqclass.demo.controller;

import com.lqclass.demo.domain.User;
import com.lqclass.demo.service.UserService;
import com.lqclass.demo.utils.JsonData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 标记这是一个异常处理类
 */
@RestController
@RequestMapping("/api/v1/pub/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("login")
    public JsonData login(@RequestBody User user){

        String token = userService.login(user.getUsername(), user.getPwd());
        System.out.println("1");
        return token != null ? JsonData.buildSuccess(token) : JsonData.buildError("账号或者密码错误！");
    }

    @GetMapping("list")
    public JsonData list(){
        return JsonData.buildSuccess(userService.list());
    }

    @GetMapping("test_error")
    public JsonData testError(){

        int res = 1 / 0;
        return JsonData.buildSuccess("right");
    }
}

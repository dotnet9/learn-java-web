package com.lqclass.demo.mapper;

import com.lqclass.demo.domain.User;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class UserMapper {

    private static Map<String, User> userMap = new HashMap<>();

    static {
        userMap.put("henry", new User(1, "henry", "123"));
        userMap.put("dotnet9", new User(2, "dotnet9", "123"));
        userMap.put("lqclass", new User(3, "lqclass", "123"));
    }

    public User login(String username, String pwd){
        User user = userMap.get(username);
        if(user == null){
            return null;
        }
        if(user.getPwd().equals(pwd)){
            return user;
        }
        return null;
    }

    public List<User> listUser(){

        List<User> list = new ArrayList<>();
        list.addAll(userMap.values());

        return list;
    }
}

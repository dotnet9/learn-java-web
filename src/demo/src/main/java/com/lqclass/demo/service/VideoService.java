package com.lqclass.demo.service;

import com.lqclass.demo.domain.Video;

import java.util.List;

public interface VideoService {

    List<Video> listVideo();

}

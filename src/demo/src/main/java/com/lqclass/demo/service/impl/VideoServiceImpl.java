package com.lqclass.demo.service.impl;

import com.lqclass.demo.domain.Video;
import com.lqclass.demo.mapper.VideoMapper;
import com.lqclass.demo.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VideoServiceImpl implements VideoService {

    @Autowired
    private VideoMapper videoMapper;

    @Override
    public List<Video> listVideo() {
        return videoMapper.listVideo();
    }
}
